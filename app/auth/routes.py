from flask import render_template, flash, redirect, url_for, request, session
from werkzeug.urls import url_parse
from flask_login import current_user, login_user, logout_user
from flask_babel import _
from app import db
from app.auth import bp
from app.models import User, ActionType, AuthLog
from app.auth.forms import LoginForm, RegistrationForm, \
    ResetPasswordRequestForm, ResetPasswordForm
from app.auth.email import send_password_reset_email
from app.linebreak import nl2br


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(db.or_(
            User.username == form.username.data,
            User.email == form.username.data
        )).first()

        if user is None or not user.check_password(form.password.data):
            flash(_('Invalid username or password'))
            return redirect(url_for('auth.login'))

        login_user(user, remember=form.remember_me.data)
        flash(_('Welcome back, %(username)s!', username=user.username))

        action_type = ActionType.query.filter_by(name='AUTH_SUCCESS').first()

        log = AuthLog(user=current_user, action=action_type,
                      ip=request.remote_addr,
                      user_agent=request.headers.get('User-Agent'),
                      session_id=session['_id'])
        db.session.add(log)
        db.session.commit()
        redirect_page = request.args.get('redirect')
        if not redirect_page or url_parse(redirect_page).netloc != '':
            redirect_page = url_for('index')

        return redirect(url_for('main.index'))

    return render_template('auth/login.html', title=_('Sign In'), form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)

        db.session.add(user)
        db.session.commit()

        flash(_('Congratulations, you are registered user now!'))
        return redirect(url_for('auth.login'))

    return render_template('auth/register.html', title=_('Register'), form=form)


@bp.route('/reset_password_request', methods=['POST', 'GET'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
            pass

        flash(_('If you have written the correct address, we will send the '
              'instructions to reset your password to this email address.'))
        return redirect(url_for('auth.login'))

    return render_template('auth/reset_password_request.html',
                           title=_('Reset Password'), form=form)


@bp.route('/reset_password/<token>', methods=['POST', 'GET'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    user = User.verify_reset_password(token)
    if not user:
        return redirect(url_for('main.index'))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash(_('Your password has been reset.'))
        return redirect(url_for('main.index'))
    return render_template('auth/reset_password.html',
                           title=_('Reset password'), form=form)
