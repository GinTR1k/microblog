from flask_mail import Message
from threading import Thread
from app import current_app, mail


def send_async_mail(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=('', sender), recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)
    Thread(target=send_async_mail,
           args=(current_app._get_current_object(), msg)).start()
